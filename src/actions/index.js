let nextReferralId = 0;

export const addReferral = referral => ({
  type: "ADD_REFERRAL",
  referral: { id: nextReferralId++, ...referral }
});

export const removeReferral = id => ({
  type: "REMOVE_REFERRAL",
  id
});

export const sendReferrals = () => ({
  type: "SEND_REFERRALS"
});

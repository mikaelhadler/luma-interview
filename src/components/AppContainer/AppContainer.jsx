import React, { useState } from "react";
import PropTypes from "prop-types";
import { sendReferrals } from "../../actions";
import "./AppContainer.scss";
import { connect } from "react-redux";

import { Grid, Container, Button } from "@material-ui/core";
import AppAddReferral from "containers/AppAddReferral.jsx";
import AppListReferral from "components/AppListReferral/AppListReferral.jsx";

export const _sendReferrals = (sendReferrals, setFinish) => {
  setFinish(true);
};
export const AppContainer = ({ listReferrals, sendReferrals }) => {
  const [finish, setFinish] = useState(false);
  return (
    <Grid container className="App-Container">
      <Grid item xs={12}>
        <h2>Referral Patients</h2>
        <h3>You can add up to five patients at a time</h3>
        {!finish && (
          <Container maxWidth="sm">
            <AppListReferral listReferrals={listReferrals} />
            <AppAddReferral />
            <Button
              className={"App-Button"}
              fullWidth
              variant={"contained"}
              onClick={() => _sendReferrals(sendReferrals, setFinish)}
            >
              SEND REFERRALS
            </Button>
          </Container>
        )}
        {finish && (
          <Container maxWidth="sm">
            Success! You have submitted{" "}
            {listReferrals.length === 0
              ? listReferrals.length + 1
              : listReferrals.length}{" "}
            pending referrals. You will be notified once they've been approved
          </Container>
        )}
      </Grid>
    </Grid>
  );
};

const mapStateToProps = state => ({
  listReferrals: state.listReferrals
});

const mapDispatchToProps = dispatch => ({
  sendReferrals: () => dispatch(sendReferrals())
});

AppContainer.propTypes = {
  listReferrals: PropTypes.array.isRequired
};
export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);

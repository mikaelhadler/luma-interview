import React, { useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import { connect } from "react-redux";
import clsx from "clsx";
import {
  Card,
  CardHeader,
  CardContent,
  Collapse,
  IconButton,
  FormControl,
  TextField,
  InputAdornment,
  ButtonBase
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import DeleteIcon from "@material-ui/icons/Delete";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import CakeIcon from "@material-ui/icons/Cake";
import TranslateIcon from "@material-ui/icons/Translate";

const useStyles = makeStyles(theme => ({
  root: {
    maxWidth: 782
  },
  alert: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2)
    }
  },
  title: {
    textAlign: "left"
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest
    })
  },
  form: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1)
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  margin: {
    margin: theme.spacing(1)
  },
  paddingTop: 0
}));

export const insertReferral = (referral, handleAddReferral, setError) => {
  if (
    referral.firstName.length === 0 ||
    referral.lastName.length === 0 ||
    referral.birthday.length === 0 ||
    referral.contactLanguage.length === 0 ||
    referral.address.length === 0
  ) {
    setError(true);
    setTimeout(() => {
      setError(false);
    }, 2000);
    return;
  }
  handleAddReferral(referral);
};

const handleExpandClick = (setExpanded, expanded) => {
  setExpanded(!expanded);
};

const handleChange = (event, set) => {
  set(event.target.value);
};

export const AppCard = ({
  title,
  index,
  initialExpanded,
  handleAddReferral,
  handleRemoveReferral,
  hiddenButton,
  referral = {},
  listReferrals
}) => {
  const classes = useStyles();
  const [error, setError] = useState(false);
  const [expanded, setExpanded] = useState(initialExpanded);
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [birthday, setBirthday] = useState("");
  const [contactLanguage, setContactLanguage] = useState("");
  const [address, setAddress] = useState("");
  const [notesReason, setNotesReason] = useState("");

  return (
    <>
      <Card className={(classes.root, classes.title)}>
        <CardHeader
          avatar={index + 1}
          action={
            <>
              {handleRemoveReferral && (
                <IconButton
                  onClick={() => handleRemoveReferral(index)}
                  aria-expanded={expanded}
                >
                  <DeleteIcon />
                </IconButton>
              )}
              <IconButton
                className={clsx(classes.expand, {
                  [classes.expandOpen]: expanded
                })}
                onClick={() => handleExpandClick(setExpanded, expanded)}
                aria-expanded={expanded}
              >
                <ExpandMoreIcon />
              </IconButton>
            </>
          }
          title={title}
        />
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            <FormControl className={classes.margin}>
              <div className={classes.form}>
                <TextField
                  className={classes.margin}
                  id="first-name"
                  label="First Name"
                  value={referral.firstName || firstName}
                  onChange={event => handleChange(event, setFirstName)}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <AccountCircleIcon />
                      </InputAdornment>
                    )
                  }}
                  required
                />
                <TextField
                  className={classes.margin}
                  id="last-name"
                  label="Last Name"
                  value={referral.lastName || lastName}
                  onChange={event => handleChange(event, setLastName)}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <AccountCircleIcon />
                      </InputAdornment>
                    )
                  }}
                  required
                />
                <TextField
                  className={classes.margin}
                  id="birthday"
                  label="Birthday"
                  placeholder="dd/mm/yyyy"
                  value={referral.birthday || birthday}
                  onChange={event => handleChange(event, setBirthday)}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <CakeIcon />
                      </InputAdornment>
                    )
                  }}
                  required
                />
                <TextField
                  className={classes.margin}
                  id="contact-language"
                  label="Contact Language"
                  value={referral.contactLanguage || contactLanguage}
                  onChange={event => handleChange(event, setContactLanguage)}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        <TranslateIcon />
                      </InputAdornment>
                    )
                  }}
                  required
                />
                <TextField
                  className={classes.margin}
                  id="address"
                  label="Address"
                  fullWidth={true}
                  value={referral.address || address}
                  onChange={event => handleChange(event, setAddress)}
                  required
                />
                <TextField
                  className={classes.margin}
                  id="notes-reason"
                  label="Notes/Reason"
                  value={referral.notesReason || notesReason}
                  onChange={event => handleChange(event, setNotesReason)}
                  fullWidth={true}
                />
              </div>
            </FormControl>
            <div style={{ textAlign: "center", color: "red" }}>
              {error && "Please fill in the required fields"}
            </div>
          </CardContent>
        </Collapse>
      </Card>
      {!hiddenButton && listReferrals.length < 4 && (
        <ButtonBase
          className={"App-Button--base"}
          onClick={() =>
            insertReferral(
              {
                firstName,
                lastName,
                birthday,
                contactLanguage,
                address,
                notesReason
              },
              handleAddReferral,
              setError
            )
          }
        >
          + ADD ANOTHER PATIENT
        </ButtonBase>
      )}
    </>
  );
};

const mapStateToProps = state => ({
  listReferrals: state.listReferrals
});

AppCard.propTypes = {
  title: PropTypes.string.isRequired,
  index: PropTypes.number.isRequired,
  initialExpanded: PropTypes.bool.isRequired,
  handleAddReferral: PropTypes.func,
  handleRemoveReferral: PropTypes.func,
  hiddenButton: PropTypes.bool.isRequired,
  referral: PropTypes.object,
  listReferrals: PropTypes.array.isRequired
};

export default connect(mapStateToProps)(AppCard);

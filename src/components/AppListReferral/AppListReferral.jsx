import React from "react";
import { connect } from "react-redux";
import { removeReferral } from "../../actions";
import AppCard from "components/AppCard/AppCard.jsx";

export const AppListReferral = ({ listReferrals, removeReferral }) => {
  const handleRemoveReferral = id => removeReferral(id);
  return listReferrals.map((referral, index) => (
    <AppCard
      key={referral.id}
      title={referral.firstName}
      index={index}
      referral={referral}
      initialExpanded={false}
      handleRemoveReferral={handleRemoveReferral}
      hiddenButton={true}
    />
  ));
};
const mapStateToProps = state => ({
  listReferrals: state.listReferrals
});
const mapDispatchToProps = dispatch => ({
  removeReferral: id => dispatch(removeReferral(id))
});
export default connect(mapStateToProps, mapDispatchToProps)(AppListReferral);

import React from "react";

import "./App.scss";
import AppHeader from "components/AppHeader/AppHeader.jsx";
import AppContainer from "components/AppContainer/AppContainer.jsx";

export const App = () => (
  <div className="App">
    <AppHeader />
    <AppContainer />
  </div>
);

export default App;

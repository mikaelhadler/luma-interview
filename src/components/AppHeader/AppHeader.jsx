import React from "react";
import { Grid } from "@material-ui/core";
import "./AppHeader.scss";

export const Header = () => (
  <Grid container className="App-header">
    <Grid item xs={12}>
      <h1>Patient Referral Form</h1>
      <h2>Hayes Valley Health San Francisco</h2>
    </Grid>
  </Grid>
);

export default Header;

import React, { useState } from "react";
import { connect } from "react-redux";
import { addReferral } from "../actions";
import AppCard from "components/AppCard/AppCard.jsx";

export const AppAddReferral = ({ addReferal, listReferrals }) => {
  const [initialExpanded] = useState(true);
  const handleAddReferral = referral => addReferal(referral);

  return (
    <AppCard
      title="New Referral"
      index={listReferrals.length}
      initialExpanded={initialExpanded}
      handleAddReferral={handleAddReferral}
      hiddenButton={false}
    />
  );
};
const mapStateToProps = state => ({
  listReferrals: state.listReferrals
});
const mapDispatchToProps = dispatch => ({
  addReferal: referral => dispatch(addReferral(referral))
});
export default connect(mapStateToProps, mapDispatchToProps)(AppAddReferral);

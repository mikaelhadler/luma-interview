export default (state = [], action) => {
  switch (action.type) {
    case "ADD_REFERRAL":
      return [...state, action.referral];
    case "REMOVE_REFERRAL":
      state.splice(action.id, 1);
      return [...state];
    case "SEND_REFERRALS":
      return [];

    default:
      return state;
  }
};

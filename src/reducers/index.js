import { combineReducers } from "redux";
import listReferrals from "./listReferrals";

const reducers = combineReducers({
  listReferrals
});

export default reducers;
